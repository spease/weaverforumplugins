# Weaver Libraries
from weaverforum.decorators import register_template, register_view
from weaverforum.models import Plugin, Theme

# External Dependencies
from django.template import Template, Context


class QuickThemePicker(Plugin):
    def get_options_helptext(self, *args, **kwargs):
        return "Demo mode determines if the picker is collapsed by default, or immediately obvious."

    def get_default_options(self, *args, **kwargs):
        return {"demo_mode": False}

    @register_view("base")
    def add_themes_to_context(self, request, template, context):
        context['plugin_themes'] = Theme.objects.filter(active=True)
        return request, template, context

    def plugin_css(self):
        return """
#quick-theme-modal {
    position: fixed;
    padding: 15px;
    bottom: 15px;
    left: 15px;
    background: var(--bg);
    border: 4px solid var(--border);
    margin-left: 15px;
    margin-bottom: 15px;
    z-index: 50;
    }
#quick-theme-modal > h4 {
    text-align: center;
    margin-top: 0px;
    }
#quick-theme-modal > form > div, #quick-theme-modal > form {
    margin-bottom: 0px;
    margin-top: 0px;
    }
#quick-theme-button {
    position: fixed;
    left: 5px;
    bottom: 5px;
    border: 1px solid var(--text);
    border-radius: 25px;
    }
label[for="quick-theme-select"] {
    display: block;
    }
"""

    def plugin_js(self):
        c = Context({"self": self})
        t = Template(
"""
(() => {
    const quickThemeStyleSheet = document.getElementById("stylesheet");
    const quickThemeStylePreload = document.getElementById("stylesheet-preload");
    const themeURL = localStorage.getItem('quickthemeURL');
    if (themeURL !== null) {
        quickThemeStylePreload.href = themeURL;
        quickThemeStyleSheet.href = themeURL;
    }

    quickThemeDemo = "{{ self.options.demo_mode }}" == "True";

    document.addEventListener('DOMContentLoaded', function() {
        const quickThemeButton = document.getElementById("quick-theme-button");
        const quickThemeSelect = document.getElementById("quick-theme-select");
        const quickThemeModal = document.getElementById("quick-theme-modal");

        if (localStorage.getItem('quicktheme') == null) {
            const quickThemeDefault = document.querySelector('select#quick-theme-select option[selected]');
            localStorage.setItem('quicktheme', quickThemeDefault.getAttribute('theme_id'));
            localStorage.setItem('quickthemeURL', quickThemeDefault.value);
        }

        quickThemeSelect.addEventListener("change", () => {
          quickThemeStyleSheet.href = quickThemeSelect.value;
          const theme = quickThemeSelect.options[quickThemeSelect.selectedIndex];
          localStorage.setItem('quicktheme', theme.getAttribute('theme_id'));
          localStorage.setItem('quickthemeURL', theme.value);
        });

        if (window.location.pathname == "/profiles/edit") {
            const accountSettingsThemePicker = document.getElementById("id_theme");
            accountSettingsThemePicker.addEventListener("change", () => {
                quickThemeStyleSheet.href = quickThemeSelect.value;
                quickThemeSelect.value = accountSettingsThemePicker.value;
            });
        }

        const themeID = localStorage.getItem('quicktheme');
        const selector = 'select#quick-theme-select option[theme_id="' + themeID + '"]';
        const startTheme = document.querySelector(selector);
        quickThemeSelect.value = startTheme.value;
        quickThemeSelect.dispatchEvent(new Event('change'));

        if (quickThemeDemo) {
            quickThemeButton.hidden = true;
            quickThemeModal.show();
        } else {
            // Update button opens a modeless dialog
            quickThemeButton.addEventListener("click", () => {
            if (quickThemeModal.open) {
                quickThemeModal.close()
            } else {
                quickThemeModal.show();
            }
            });
        }
    });
  })();
""")
        return t.render(c)


    @register_template("base.footer")
    def show_quickthemepicker(self):
        demo_state = "Demo " if self.options['demo_mode'] else ""
        return """
<button id="quick-theme-button">🎨</button>
<dialog id="quick-theme-modal">
    <h4>""" + demo_state + """Quick Theme Picker</h4>
    <form>
        <div>
            <select id="quick-theme-select">
                {% for theme in plugin_themes %}
                <option theme_id="{{theme.id}}" value="{{theme.get_filename}}"
                    {% if profile.get_theme_id == theme.id%} selected{% endif %}>{{ theme.name }}</options>
                {% endfor %}
            </select>
            <label for="quick-theme-select">Theme</label>
        </div>
    </form>
</dialog>
"""