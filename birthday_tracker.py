# Weaver Libraries
from weaverforum.admin import PluginModelAdmin
from weaverforum.decorators import register_form, register_template, register_view
from weaverforum.forms import PluginForm
from weaverforum.models import Plugin, PluginModel

# External Dependencies
from django import forms
from django.conf import settings
from django.contrib import admin
from django.db import models
from django.utils import timezone


class Birthday(PluginModel):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=False, blank=False, related_name="birthday"
    )
    date = models.DateField(null=True)

    def __str__(self):
        return f"{str(self.user)}'s Birthday"

    @property
    def is_birthday(self):
        today = timezone.now()
        if self.date:
            return today.month == self.date.month and today.day == self.date.day
        return False

    @property
    def age(self):
        today = timezone.now()
        return today.year - self.date.year


@admin.register(Birthday)
class BirthdayTrackerAdmin(PluginModelAdmin):
    fields = ["user", "date"]
    list_display = ["user", "date"]
    readonly_fields = ["user"]


class BirthdayTracker(Plugin):
    models = [
        Birthday,
    ]
    urls = None

    def get_options_helptext(self, *args, **kwargs):
        return "The birthday plugin works out of the box."

    def get_default_options(self, *args, **kwargs):
        return {"options": None}

    @register_view("edit_settings")
    def save_dob(self, request, template, context=None):
        if context:
            form = context["form"]
        else:
            context = {}

        if request.method == "POST" and context:
            if form.is_valid():
                defaults = {"date": form.cleaned_data["birthday"]}
                Birthday.objects.update_or_create(user=request.user, defaults=defaults)
        return request, template, context

    @register_form("EditSettingsForm")
    class BirthdayForm(PluginForm):
        birthday = forms.DateField(
            label="Birthday", required=False, widget=forms.DateInput(attrs={"type": "date"})
        )

        def __init__(self, data=None, **kwargs):
            if "birthday" not in data:
                data["birthday"] = Birthday.objects.get_or_create(user=data["request"].user)[0].date
            super().__init__(data, **kwargs)

    @register_view("view_index")
    def add_birthday_context(self, request, template, context=None):
        if not context:
            context = {}
        today = timezone.now()
        birthdays = Birthday.objects.filter(date__month=today.month, date__day=today.day)
        context["birthdays"] = birthdays
        return request, template, context

    def plugin_css(self):
        return """
article.birthdays {
    margin-top: 3rem;
    background-color: var(--header);
    border-color: transparent;
}
    """

    @register_template("view_forum_index.footer")
    def show_birthdays(self):
        return """
    {% if birthdays|length > 0 %}
    <article class="birthdays">
        <div class="col">
            <span class="forum-name">Birthdays Today</span>
        </div>
        <div class="col">
            {% for plugin_model in birthdays %}
            <a href="{% url 'view_profile' plugin_model.user.id %}"
               class="rankstyle-{{ plugin_model.user.profile.rank.id }} bold">
                {{plugin_model.user.username }}
            </a> ({{ plugin_model.age }}){% if not forloop.last %},{% endif %}
            {% endfor %}
        </div>
    </article>
    {% endif %}
    """

    @register_template("view_post.username")
    def show_birthday_icon(self):
        return """{% if created_by.birthday and created_by.birthday.is_birthday %}🎂{% endif %}"""

    @register_template("view_profile.username")
    def profile_show_birthday_icon(self):
        return """
    {% if profile.user.birthday and profile.user.birthday.is_birthday %}
        <b title="It's {{ profile.user.username }}'s birthday today!">🎂</b>
    {% endif %}
    """