# Weaver Forum Plugins

[![license BSD](https://img.shields.io/badge/license-BSD-lightgrey.svg)](https://gitlab.com/spease/WeaverForumSoftware/blob/master/LICENSE)
[![demo link](https://img.shields.io/badge/demo-availible-brightgreen.svg)](https://weaverforum.com)
[![weaver pun](https://img.shields.io/badge/forum%20threads-weaved-brightgreen.svg)](https://weaverforum.com)

*Weaver Forum Plugins* are the default plugins for Weaver Forum Software, a
lightweight, cutomizable forum solution designed to run with minimal processing 
power, space, and bandwidth requirements.